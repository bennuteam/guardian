FROM golang:1.11-alpine
WORKDIR /go/src/gitlab.com/bennuteam/guardian
COPY  . .
RUN adduser -u 1000 -S -D -H guardian && \
    cd cmd/guardian && \
    export GIT_SHA=$(git rev-parse --short=8 HEAD) && \
    export GIT_TAG=$(git describe --tags --abbrev=0 --exact-match 2>/dev/null) && \
    export GIT_DIRTY=$(test -n "`git status --porcelain`" && echo "dirty" || echo "clean") && \
    CGO_ENABLED=0 GOOS=linux go build -a -ldflags="-d -s -w -extldflags '-static' -X gitlab.com/bennuteam/guardian/internal/guardian.GitCommit=${GIT_SHA} -X gitlab.com/bennuteam/guardian/internal/guardian.GitTreeState=${GIT_DIRTY} -X gitlab.com/bennuteam/guardian/internal/guardian.GitTag=${GIT_TAG} -X gitlab.com/bennuteam/guardian/internal/guardian.Version=${GIT_TAG}" -installsuffix cgo -o guardian

FROM scratch
ADD https://raw.githubusercontent.com/containous/traefik/master/script/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=0 /go/src/gitlab.com/bennuteam/guardian/cmd/guardian/guardian .
COPY --from=0 /etc/passwd /etc/passwd
USER guardian
ENTRYPOINT ["/guardian"]