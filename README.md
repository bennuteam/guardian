# No LB for SVCs

Do not allow creation of svc of `type: LoadBalancer` on any namespace, unless you are a k8s admin.

## Attribution

This project uses the upstream examples found in the following repos:
* https://github.com/caesarxuchao/example-webhook-admission-controller
* https://github.com/kubernetes/kubernetes/tree/release-1.9/test/images/webhook

## Deploy

```sh
# Simply run
$ ./scripts/guardian.sh && kubectl apply -f deploy/manifest.yaml
```