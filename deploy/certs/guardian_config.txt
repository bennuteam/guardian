[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
[ v3_req ]
basicConstraints=CA:FALSE
subjectAltName=@alt_names
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth

[ alt_names ]
DNS.1 = guardian
DNS.2 = guardian.kube-system
DNS.3 = guardian.kube-system.svc
DNS.4 = guardian.kube-system.svc.cluster.local
