module gitlab.com/bennuteam/guardian

require (
	github.com/beorn7/perks v0.0.0-20180321164747-3a771d992973
	github.com/gogo/protobuf v1.1.1
	github.com/golang/protobuf v1.3.0
	github.com/google/gofuzz v0.0.0-20170612174753-24818f796faf
	github.com/gorilla/mux v1.7.0
	github.com/json-iterator/go v1.1.5
	github.com/matttproud/golang_protobuf_extensions v1.0.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd
	github.com/modern-go/reflect2 v1.0.1
	github.com/opentracing/opentracing-go v1.0.2
	github.com/prometheus/client_golang v0.9.2
	github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90
	github.com/prometheus/common v0.0.0-20190124163007-cfeb6f9992ff
	github.com/prometheus/procfs v0.0.0-20190227231451-bbced9601137
	github.com/slok/kubewebhook v0.2.0
	golang.org/x/net v0.0.0-20190301231341-16b79f2e4e95
	golang.org/x/text v0.3.0
	gopkg.in/inf.v0 v0.9.1
	gopkg.in/yaml.v2 v2.2.2
	k8s.io/api v0.0.0-20190301173355-16f65c82b8fa
	k8s.io/apimachinery v0.0.0-20190301173222-2f7e9cae4418
	k8s.io/klog v0.0.0-20181108234604-8139d8cb77af
	sigs.k8s.io/yaml v1.1.0
)
