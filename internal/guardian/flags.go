package guardian

import (
	"flag"
	"os"

	"gitlab.com/bennuteam/guardian/pkg/types"
)

// InitConfig initializes config for guardian
func InitConfig() *types.Guardian {
	guard := &types.Guardian{}
	fl := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	fl.BoolVar(&guard.Debug, "debug", true, "Debug mode")
	fl.UintVar(&guard.Port, "port", 8443, "Port to serve admission controller")
	fl.StringVar(&guard.TLSCertFile, "pubkey", "/etc/certs/tls.crt", "Path to TLS public key")
	fl.StringVar(&guard.TLSKeyFile, "privkey", "/etc/certs/tls.key", "Path to TLS private key")
	fl.Parse(os.Args[1:])
	return guard
}
