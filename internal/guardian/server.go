package guardian

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	whhttp "github.com/slok/kubewebhook/pkg/http"
	"github.com/slok/kubewebhook/pkg/log"
	validator "github.com/slok/kubewebhook/pkg/webhook/validating"
	"gitlab.com/bennuteam/guardian/pkg/tools"
	"gitlab.com/bennuteam/guardian/pkg/types"
	"k8s.io/api/core/v1"
)

var (
	// Version of Guardian
	Version string
	// GitTag is the tag the binary was built from
	GitTag string
	// GitCommit is the commit sha  the binary was built from
	GitCommit string
	// GitTreeState is the state of the branch the binary was built from
	GitTreeState string
)

func Server() {
	guard := InitConfig()

	logger := &log.Std{Debug: guard.Debug}
	logger.Infof("Guardian Admission controller\n")
	logger.Infof("Version: %s\n", Version)
	logger.Infof("GitTag: %s\n", GitTag)
	logger.Infof("GitCommit: %s\n", GitCommit)
	logger.Infof("GitTreeState: %s\n", GitTreeState)

	certs, err := tls.LoadX509KeyPair(guard.TLSCertFile, guard.TLSKeyFile)
	if err != nil {
		logger.Errorf("Failed to load keypair: %v", err)
		os.Exit(1)
	}

	vl := &types.ServiceValidator{
		Logger: logger,
	}

	vcfg := validator.WebhookConfig{
		Name: "ServiceValidator",
		Obj:  &v1.Service{},
	}

	wh, err := validator.NewWebhook(vcfg, vl, nil, nil, logger)
	if err != nil {
		logger.Errorf("Error creating webhook %v", err)
		os.Exit(1)
	}

	logger.Infof("Starting server...\n")

	r := mux.NewRouter()
	r.Handle("/validate", whhttp.MustHandlerFor(wh))
	r.Handle("/healthz", guard.Healthz())

	nextRequestID := func() string {
		return fmt.Sprintf("%d", time.Now().UnixNano())
	}

	server := &http.Server{
		Addr:         fmt.Sprintf(":%v", guard.Port),
		TLSConfig:    &tls.Config{Certificates: []tls.Certificate{certs}},
		Handler:      tools.Tracer(nextRequestID)(tools.Logging(logger)(r)),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	logger.Infof("Listening on: %v", guard.Port)

	// Shutdown signal
	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	go func() {
		<-quit
		logger.Infof("Server is shutting down...\n")
		atomic.StoreInt32(&guard.Healthy, 0)

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			logger.Errorf("Could not gracefully shutdown the server: %v\n", err)
		}
		close(done)
	}()

	logger.Infof("Server is ready to handle requests at %v\n", guard.Port)
	atomic.StoreInt32(&guard.Healthy, 1)
	if err := server.ListenAndServeTLS("", ""); err != http.ErrServerClosed {
		logger.Errorf("Could not start server on %s: %v\n", guard.Port, err)
		os.Exit(1)
	}
	<-done
	logger.Infof("Server stopped")
}
