package types

import (
	"context"
	"fmt"
	"net/http"
	"sync/atomic"

	"github.com/slok/kubewebhook/pkg/log"
	validator "github.com/slok/kubewebhook/pkg/webhook/validating"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Guardian contains configs for admission controller
type Guardian struct {
	Debug                   bool
	Healthy                 int32
	Port                    uint
	TLSCertFile, TLSKeyFile string
}

// Healthz is an endpoint for liveness
func (guard *Guardian) Healthz() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if atomic.LoadInt32(&guard.Healthy) == 1 {
			w.WriteHeader(http.StatusOK)
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}

type ServiceValidator struct {
	Logger log.Logger
}

func (v *ServiceValidator) Validate(ctx context.Context, obj metav1.Object) (bool, validator.ValidatorResult, error) {
	svc, ok := obj.(*v1.Service)

	if !ok {
		return false, validator.ValidatorResult{}, fmt.Errorf("Not a service")
	}

	if svc.Spec.Type == "LoadBalancer" {
		v.Logger.Infof("Checking service %s/%s...\n", svc.Namespace, svc.Name)
		v.Logger.Infof("Service %s/%s is type LoadBalancer\n", svc.Namespace, svc.Name)
		nolb := svc.Annotations["guardian.ac/lb"]
		if nolb == "" || nolb != "true" {
			res := validator.ValidatorResult{
				Valid:   false,
				Message: "Not allowed to deploy services of type LoadBalancer",
			}
			return false, res, nil
		}
	}

	v.Logger.Infof("Service %s/%s is valid\n", svc.Namespace, svc.Name)
	res := validator.ValidatorResult{
		Valid:   true,
		Message: "Service is not of type LoadBalancer",
	}
	return false, res, nil
}
