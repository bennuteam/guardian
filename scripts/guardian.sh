#!/bin/bash

export HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
export BRANCH=$(git branch | grep \* | cut -d ' ' -f2)
export SHA=$(git rev-parse --short=8 HEAD)
export DOCKER_TAG=${BRANCH}-${SHA}

set -o errexit
set -o nounset
set -o pipefail


# CREATE THE PRIVATE KEY FOR OUR CUSTOM CA
openssl genrsa -out ${HERE}/../deploy/certs/ca.key 2048

# GENERATE A CA CERT WITH THE PRIVATE KEY
openssl req -new -x509 -key ${HERE}/../deploy/certs/ca.key -out ${HERE}/../deploy/certs/ca.crt -config ${HERE}/../deploy/certs/ca_config.txt

# CREATE THE PRIVATE KEY FOR OUR guardian SERVER
openssl genrsa -out ${HERE}/../deploy/certs/guardian-key.pem 2048

# CREATE A CSR FROM THE CONFIGURATION FILE AND OUR PRIVATE KEY
openssl req -new -key ${HERE}/../deploy/certs/guardian-key.pem -subj "/CN=guardian.kube-system.svc" -out guardian.csr -config ${HERE}/../deploy/certs/guardian_config.txt

# CREATE THE CERT SIGNING THE CSR WITH THE CA CREATED BEFORE
openssl x509 -req -in guardian.csr -CA ${HERE}/../deploy/certs/ca.crt -CAkey ${HERE}/../deploy/certs/ca.key -CAcreateserial -out ${HERE}/../deploy/certs/guardian-crt.pem

# INJECT CA IN THE WEBHOOK CONFIGURATION
export CERT_SECRET_HASH=$(< /dev/urandom tr -dc a-z0-9 | head -c${1:-8})
export PRIV_KEY=$(cat ${HERE}/../deploy/certs/guardian-key.pem|base64|tr -d '\n')
export PUB_KEY=$(cat ${HERE}/../deploy/certs/guardian-crt.pem|base64|tr -d '\n')
export CA_BUNDLE=$(cat ${HERE}/../deploy/certs/ca.crt|base64|tr -d '\n')
cat ${HERE}/../deploy/_manifest_.yaml | envsubst > ${HERE}/../deploy/manifest.yaml
